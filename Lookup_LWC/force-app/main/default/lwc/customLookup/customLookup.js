import { LightningElement, track, api } from "lwc";
import findRecords from "@salesforce/apex/CustomLookupController.findRecords";
import styling from "@salesforce/resourceUrl/customLookup";
import { loadStyle } from "lightning/platformResourceLoader";
export default class CustomLookup extends LightningElement {
  @track records;
  @track error;
  @track selectedRecord;
  @api index;
  @api relationshipfield;

  /* PASS DOWN FROM PARENT COMPONENT */
  // @api iconname = "standard:contact";
  // @api objectName = "Contact";
  // @api searchfield = "Name";

  @api iconname;
  @api objectName;
  @api searchfield;
  @api origin; //location for where the callback is coming from

  renderedCallback() {
    Promise.all([loadStyle(this, styling)])
      .then(() => {})
      .catch((error) => {
        this.error = error;
      });
  }

  async handleOnchange(event) {
    const searchKey = event.detail.value;
    try {
      const res = await findRecords({
        searchKey: searchKey,
        objectName: this.objectName,
        searchField: this.searchfield
      });
      this.records = res;
      this.error = undefined;
    } catch (e) {
      this.error = error;
      this.records = undefined;
    }
  }
  handleSelect(event) {
    const selectedRecordId = event.detail;
    this.selectedRecord = this.records.find(
      (record) => record.Id === selectedRecordId
    );

    /* fire the event with the value of RecordId for the Selected RecordId */
    const selectedRecordEvent = new CustomEvent("selectedrec", {
      detail: {
        recordId: selectedRecordId,
        index: this.index,
        relationshipfield: this.relationshipfield
      }
    });
    this.dispatchEvent(selectedRecordEvent);

    if (selectedRecordId !== null) {
      this.notifyParent(selectedRecordId);
    }
  }

  notifyParent(selectedRecordId) {
    const selectedEvent = new CustomEvent("idchange", {
      detail: {
        id: selectedRecordId,
        origin: this.origin
      }
    });
    this.dispatchEvent(selectedEvent);
  }

  handleRemove(event) {
    event.preventDefault();
    this.selectedRecord = undefined;
    this.records = undefined;
    this.error = undefined;
    /* fire the event with the value of undefined for the Selected RecordId */
    const selectedRecordEvent = new CustomEvent("selectedrec", {
      detail: {
        recordId: undefined,
        index: this.index,
        relationshipfield: this.relationshipfield
      }
    });
    this.dispatchEvent(selectedRecordEvent);
  }
}
