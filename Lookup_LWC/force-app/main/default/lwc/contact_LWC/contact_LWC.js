import { LightningElement } from "lwc";

export default class Contact_LWC extends LightningElement {
  contact = "Contact";
  searchField = "Name";
  icon = "standard:contact";
  origin = "contact_lwc";

  handleRecordChange(event) {
    switch (event.detail.origin) {
      default:
        console.log("no cases");
    }
  }
}
