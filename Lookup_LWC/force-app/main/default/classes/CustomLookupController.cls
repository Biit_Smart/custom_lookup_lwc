public with sharing class CustomLookupController {
@AuraEnabled(cacheable = true)
public static List<SObject> findRecords(String searchKey, String objectName, String searchField){
	String key = '%' + searchKey + '%';
	String query = 'SELECT Id, '+searchField+' FROM '+objectName +' WHERE '+searchField +' LIKE :key LIMIT 10';
	System.debug(System.LoggingLevel.DEBUG, query);
	List<SObject> sObjectList = Database.query(query);
	return sObjectList;
}
}