# README #

Re-usable LWC component for lookups

![output](https://i.imgur.com/RCn6SWr.png)

### What is this repository for? ###

Sadly Salesforce does not provide us with a standard component for something as common as a lookup. So here is a LWC
that can be used, code can be copied and cloned and used as a reference. Do keep in mind that there is a static resource 
file with some additional CSS that you might need.

```ruby
<c-custom-lookup
    onidchange={handleRecordChange}
    object-name={contact}
    search-field={searchField}
    icon-name={icon}
    origin={origin}
  >
</c-custom-lookup>
```